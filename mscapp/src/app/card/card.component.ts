import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.scss']
})
export class CardComponent implements OnInit {

  image = 'http://math.sci.ubu.ac.th/assets/staffimages/23.png';
  name = 'Chayaporn Kansan';
  position = 'teacher'

  constructor() { }

  ngOnInit() {
  }

}
